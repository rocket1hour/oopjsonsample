package home.oop.benxe.services;

import java.util.Map;

import home.oop.benxe.biz.ChuyenXeBiz;
import home.oop.benxe.entity.ChuyenXe;

public class ChuyenXeServicesImpl implements ChuyenXeServices {

    @Override
    public Map<String, ChuyenXe> getAllChuyenXe() {
        Map<String, ChuyenXe> map = ChuyenXeBiz.readChuyenXes("");
        return map;
    }

    @Override
    public ChuyenXe getChuyenXe(String macx) {
        Map<String, ChuyenXe> map = ChuyenXeBiz.readChuyenXes(macx);
        if(map.isEmpty())
            return null;
        return (ChuyenXe) map.values().toArray()[0];
    }

    @Override
    public ChuyenXe addChuyenXe(ChuyenXe chuyenXe) {
        try {
            boolean isSuccess = ChuyenXeBiz.changeChuyenXes(chuyenXe, 1);
            if(isSuccess)
                return chuyenXe;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ChuyenXe updateChuyenXe(ChuyenXe chuyenXe) {
        try {
            boolean isSuccess = ChuyenXeBiz.changeChuyenXes(chuyenXe, 2);
            if(isSuccess)
                return chuyenXe;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void delChuyenXe(String macx) {
        try {
            ChuyenXeBiz.removeChuyenXes(macx, 3);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}