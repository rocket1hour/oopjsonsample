package home.oop.benxe.services;

import java.util.Map;

import home.oop.benxe.entity.ChuyenXe;

public interface ChuyenXeServices {

    public Map<String, ChuyenXe> getAllChuyenXe();

    public ChuyenXe getChuyenXe(String macx);

    public ChuyenXe addChuyenXe(ChuyenXe chuyenXe);

    public ChuyenXe updateChuyenXe(ChuyenXe chuyenXe);

    public void delChuyenXe(String macx);
}