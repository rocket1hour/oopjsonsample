package home.oop.benxe;

import java.util.ArrayList;
import java.util.List;

import home.oop.benxe.liferay.bean.PublishPermit;
import home.oop.benxe.liferay.bean.User;
import home.oop.benxe.liferay.bean.UserGroup;
import home.oop.benxe.util.LiferayUtil;

public class LiferayPermitRunning {

    public static void main(String[] args) {
        List<String> user_liferay = new ArrayList<>();
        List<String> user_permit = new ArrayList<>();

        List<String> group_liferay = new ArrayList<>();
        List<String> group_permit = new ArrayList<>();

        List<User> users = null;
        List<UserGroup> userGroups = null;
        List<PublishPermit> publishPermits = null;

        try {
            users = (List<User>) LiferayUtil.getListObject(LiferayUtil.USER, 1);
            userGroups = (List<UserGroup>) LiferayUtil.getListObject(LiferayUtil.USERGROUP, 2);
            publishPermits = (List<PublishPermit>) LiferayUtil.getListObject(LiferayUtil.PUBLISHPERMIT, 3);
        } catch (Exception e) {
            System.out.println("error" + e);
        }

        boolean isSaved = false;
        for (User user : users) {
            isSaved = false;
            PublishPermit userPermit = null;
            for (int i = 0; i < publishPermits.size(); i++) {
                userPermit = publishPermits.get(i);
                if (userPermit.getUserId() == LiferayUtil.IS_NOT_USER) {
                    continue;
                }
                if (user.getUserId() == userPermit.getUserId()) {
                    user_permit.add(user.getUserName());
                    isSaved = true;
                    break;
                }
            }
            if (!isSaved) {
                user_liferay.add(user.getUserName());
            }
        }

        for (UserGroup userGroup : userGroups) {
            isSaved = false;
            for (int i = 0; i < publishPermits.size(); i++) {
                PublishPermit groupPermit = publishPermits.get(i);
                if (groupPermit.getGroupId() == LiferayUtil.IS_NOT_GROUP) {
                    continue;
                }
                if (userGroup.getGroupId() == groupPermit.getGroupId()) {
                    group_permit.add(userGroup.getGroupName());
                    isSaved = true;
                    break;
                }
            }
            if (!isSaved) {
                group_liferay.add(userGroup.getGroupName());
            }
        }

        String a = "";
    }

}