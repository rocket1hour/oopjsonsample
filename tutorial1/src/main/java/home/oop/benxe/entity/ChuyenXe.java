package home.oop.benxe.entity;

import java.util.Scanner;

import com.google.gson.JsonObject;

public class ChuyenXe {
    protected String maCX;
    protected String tenTx;
    protected String soX;
    protected Double doanhThu;
    protected Boolean noiThanh;
    Scanner in = new Scanner(System.in);

    public ChuyenXe() {
        this.maCX = "";
        this.tenTx = "";
        this.soX = "";
        this.doanhThu = 0d;
        this.noiThanh = null;
    }

    public ChuyenXe(String maCX, String tenTx, String soX, Double doanhThu, Boolean noiThanh) {
        this.maCX = maCX;
        this.tenTx = tenTx;
        this.soX = soX;
        this.doanhThu = doanhThu;
        this.noiThanh = noiThanh;
    }

    public void callChuyenXe(JsonObject chuyenXe){
        this.maCX = chuyenXe.get("maCX").getAsString();
        this.tenTx = chuyenXe.get("tenTx").getAsString();
        this.soX = chuyenXe.get("soX").getAsString();
        this.doanhThu = chuyenXe.get("doanhThu").getAsDouble();
        this.noiThanh = chuyenXe.get("isNoiThanh").getAsBoolean();
    }

    public JsonObject bookChuyenXe(JsonObject chuyenXe){
        chuyenXe.addProperty("maCX", this.maCX);
        chuyenXe.addProperty("tenTx", this.tenTx);
        chuyenXe.addProperty("soX", this.soX);
        chuyenXe.addProperty("doanhThu", this.doanhThu);
        chuyenXe.addProperty("isNoiThanh", this.noiThanh);
        return chuyenXe;
    }

    @Override
    public String toString() {
        return 
            " maCX='" + getMaCX() + "'" +
            ", tenTx='" + getTenTx() + "'" +
            ", soX='" + getSoX() + "'" +
            ", doanhThu='" + getDoanhThu() + "'";
    }
    
    public String getMaCX() {
        return this.maCX;
    }

    public void setMaCX(String maCX) {
        this.maCX = maCX;
    }

    public String getTenTx() {
        return this.tenTx;
    }

    public void setTenTx(String tenTx) {
        this.tenTx = tenTx;
    }

    public String getSoX() {
        return this.soX;
    }

    public void setSoX(String soX) {
        this.soX = soX;
    }

    public Double getDoanhThu() {
        return this.doanhThu;
    }

    public void setDoanhThu(Double doanhThu) {
        this.doanhThu = doanhThu;
    }

    public Boolean isNoiThanh() {
        return this.noiThanh;
    }

    public void setNoiThanh(Boolean noiThanh) {
        this.noiThanh = noiThanh;
    }

    public Scanner getIn() {
        return this.in;
    }
}