package home.oop.benxe.entity;

import com.google.gson.JsonObject;

public class NoiThanh extends ChuyenXe {
    private Double soKm;
    private String soTuyen;

    public NoiThanh() {
        super();
        this.soKm = 0d;
        this.soTuyen = "";
    }

    public NoiThanh(String maCX, String tenTx, String soX, Double doanhThu, Double soKm, String soTuyen) {
        super(maCX, tenTx, soX, doanhThu, Boolean.TRUE);
        this.soKm = soKm;
        this.soTuyen = soTuyen;
    }

    @Override
    public void callChuyenXe(JsonObject chuyenXe){
        super.callChuyenXe(chuyenXe);
        this.soKm = chuyenXe.get("soKm").getAsDouble();
        this.soTuyen = chuyenXe.get("soTuyen").getAsString();
    }

    @Override
    public JsonObject bookChuyenXe(JsonObject chuyenXe) {
        super.bookChuyenXe(chuyenXe);
        chuyenXe.addProperty("soKm", this.soKm);
        chuyenXe.addProperty("soTuyen", this.soTuyen);
        return chuyenXe;
    }

    @Override
    public String toString() {
        return "Day la chuyen: " +
            super.toString() +
            " soKm='" + getSoKm() + "'" +
            ", soTuyen='" + getSoTuyen() + "'" +
            "";
    }

    public double getSoKm() {
        return this.soKm;
    }

    public void setSoKm(double soKm) {
        this.soKm = soKm;
    }

    public String getSoTuyen() {
        return this.soTuyen;
    }

    public void setSoTuyen(String soTuyen) {
        this.soTuyen = soTuyen;
    }
}