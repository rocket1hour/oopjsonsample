package home.oop.benxe.entity;

import com.google.gson.JsonObject;

public class NgoaiThanh extends ChuyenXe {
    private String noiDen;
    private Integer soNgay;

    public NgoaiThanh() {
        super();
        noiDen = "";
        soNgay = 0;
    }

    public NgoaiThanh(String maCX, String tenTx, String soX, Double doanhThu, String noiDen, Integer soNgay) {
        super(maCX, tenTx, soX, doanhThu, Boolean.FALSE);
        this.noiDen = noiDen;
        this.soNgay = soNgay;
    }

    public void callChuyenXe(JsonObject chuyenXe){
        super.callChuyenXe(chuyenXe);
        this.noiDen = chuyenXe.get("noiDen").getAsString();
        this.soNgay = chuyenXe.get("soNgay").getAsInt();
    }

    @Override
    public JsonObject bookChuyenXe(JsonObject chuyenXe) {
        super.bookChuyenXe(chuyenXe);
        chuyenXe.addProperty("noiDen", this.noiDen);
        chuyenXe.addProperty("soNgay", this.soNgay);
        return chuyenXe;
    }

    @Override
    public String toString() {
        return "Day la chuyen: " +
            super.toString() +
            " noiDen='" + getnoiDen() + "'" +
            ", soNgay='" + getsoNgay() + "'" +
            "";
    }

    public String getnoiDen() {
        return this.noiDen;
    }

    public void setnoiDen(String noiDen) {
        this.noiDen = noiDen;
    }

    public int getsoNgay() {
        return this.soNgay;
    }

    public void setsoNgay(int soNgay) {
        this.soNgay = soNgay;
    }
}