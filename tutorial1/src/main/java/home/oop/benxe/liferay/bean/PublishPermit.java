package home.oop.benxe.liferay.bean;

import java.util.Objects;

public class PublishPermit {
    private Integer userId;
    private Integer groupId;
    private String creator;

    public PublishPermit() {
    }
    

    public PublishPermit(Integer userId, Integer groupId, String creator) {
        this.userId = userId;
        this.groupId = groupId;
        this.creator = creator;
    }

    public Integer getUserId() {
        return this.userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getGroupId() {
        return this.groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public String getCreator() {
        return this.creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }
    
    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof PublishPermit)) {
            return false;
        }
        PublishPermit publishPermit = (PublishPermit) o;
        return Objects.equals(userId, publishPermit.userId) && Objects.equals(groupId, publishPermit.groupId) && Objects.equals(creator, publishPermit.creator);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, groupId, creator);
    }

    @Override
    public String toString() {
        return "{" +
            " userId='" + getUserId() + "'" +
            ", groupId='" + getGroupId() + "'" +
            ", creator='" + getCreator() + "'" +
            "}";
    }
    
}