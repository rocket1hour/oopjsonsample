package home.oop.benxe.liferay.bean;

import java.util.Objects;

public class UserGroup {
    private Integer groupId;
    private String groupName;

    public UserGroup() {
    }

    public UserGroup(Integer groupId, String groupName) {
        this.groupId = groupId;
        this.groupName = groupName;
    }

    public Integer getGroupId() {
        return this.groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return this.groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof UserGroup)) {
            return false;
        }
        UserGroup userGroup = (UserGroup) o;
        return Objects.equals(groupId, userGroup.groupId) && Objects.equals(groupName, userGroup.groupName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(groupId, groupName);
    }

    @Override
    public String toString() {
        return "{" +
            " groupId='" + getGroupId() + "'" +
            ", groupName='" + getGroupName() + "'" +
            "}";
    }
    
}