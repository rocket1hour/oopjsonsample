package home.oop.benxe;

import java.util.Map;

import home.oop.benxe.entity.ChuyenXe;
import home.oop.benxe.entity.NgoaiThanh;
import home.oop.benxe.entity.NoiThanh;
import home.oop.benxe.services.ChuyenXeServices;
import home.oop.benxe.services.ChuyenXeServicesImpl;

public class BenXe{
    public static void main(String[] args) {
        ChuyenXeServices chuyenXeServices = new ChuyenXeServicesImpl();
        
        Map<String, ChuyenXe> map = chuyenXeServices.getAllChuyenXe();
        // get All
        map.forEach((k,v)->System.out.println("Key : " + k + " Value : " + v.toString()));
        // get One
        ChuyenXe aChuyenXe = chuyenXeServices.getChuyenXe("001");
        if(aChuyenXe != null)
            System.out.println("Key : " + aChuyenXe.getMaCX() + " Value : " + aChuyenXe.toString());
        
        // add
        ChuyenXe cxA = new NoiThanh("006","nguyen van f", "",11234d, 44d, "52");
        ChuyenXe cxB = new NgoaiThanh("007","nguyen van g", "" ,11d, "", 5);
        cxA = chuyenXeServices.addChuyenXe(cxA);
        if(cxA != null){
            System.out.println(cxA.toString());
        }
        cxB = chuyenXeServices.addChuyenXe(cxB);
        if(cxB != null){
            System.out.println(cxB.toString());
        }
        // update
        ChuyenXe cxC = new NoiThanh("004","nguyen van i", "",11234d, 44d, "52");
        cxC = chuyenXeServices.updateChuyenXe(cxC);
        if(cxC != null){
            System.out.println(cxC.toString());
        }

        //delete 
        String removeChuyenXe = "006";
        chuyenXeServices.delChuyenXe(removeChuyenXe);
        map = chuyenXeServices.getAllChuyenXe();
        map.forEach((k,v)->System.out.println("Key : " + k + " Value : " + v.toString()));        
    }
}