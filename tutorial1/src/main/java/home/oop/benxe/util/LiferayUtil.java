package home.oop.benxe.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.lang.Class;
import java.nio.charset.StandardCharsets;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import home.oop.benxe.liferay.bean.PublishPermit;
import home.oop.benxe.liferay.bean.User;
import home.oop.benxe.liferay.bean.UserGroup;

import java.lang.reflect.Type;

public class LiferayUtil {
    public static final String USERGROUP = LiferayUtil.LIFERAY_JSON_CONTEXT_PATH + "usergroup.json";
    public static final String USER = LiferayUtil.LIFERAY_JSON_CONTEXT_PATH + "user.json";
    public static final String PUBLISHPERMIT = LiferayUtil.LIFERAY_JSON_CONTEXT_PATH + "publishpermit.json";
    public static final String LIFERAY_JSON_CONTEXT_PATH = "/xes/liferay/";
    public static final Integer IS_NOT_USER = 0;
    public static final Integer IS_NOT_GROUP = 0;

    private static final String PATH_CLASS_LIFERAY = "home.oop.benxe.liferay.bean";
    private static final String CLASS_USER = ".User";
    private static final String CLASS_USERGROUP = ".UserGroup";
    private static final String CLASS_PUBLISHPERMIT = ".PublishPermit";

    public File getFileFromResources(String fileName) {
        try {
            File file = new File(getClass().getClassLoader().getResource(fileName).toURI());
            return file;
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    public String getInputStreamFromResources(String fileName) {
        try {
            // Read file json
            InputStream dataInitStream = LiferayUtil.class.getResourceAsStream(fileName);
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            int nRead;
            byte[] data = new byte[1024];
            while ((nRead = dataInitStream.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, nRead);
            }
            buffer.flush();
            byte[] byteArray = buffer.toByteArray();
            String content = new String(byteArray, StandardCharsets.UTF_8);
            content = content.trim();
            return content;
        } catch (Exception e) {
            System.out.println(e);
        }
        return "";
    }

    public static List<?> getListObject(String path, Integer type) {
        String json = getNewLiferayUtil().getInputStreamFromResources(path);
        Gson gson = new Gson();
        List<?> lst = null;
        Type typeOfSrc = null;

        switch (type) {
            case 1:
                typeOfSrc = new TypeToken<List<User>>() {
                }.getType();
                break;
            case 2:
                typeOfSrc = new TypeToken<List<UserGroup>>() {
                }.getType();
                break;
            case 3:
                typeOfSrc = new TypeToken<List<PublishPermit>>() {
                }.getType();
                break;
            default:
                break;
        }

        lst = gson.fromJson(json, typeOfSrc);
        return lst;
    }

    public static Object getTypeObject(Integer type) {
        Object object = null;
        try {
            switch (type) {
                case 1:
                    object = getObjectByKey(CLASS_USER);
                    break;
                case 2:
                    object = getObjectByKey(CLASS_USERGROUP);
                    break;
                case 3:
                    object = getObjectByKey(CLASS_PUBLISHPERMIT);
                    break;
                default:
                    break;
            }
        } catch (Exception ex) {
            System.out.println(ex);
        }

        return object;
    }

    private static Object getObjectByKey(String className) throws Exception {
        try {
            String classPath = PATH_CLASS_LIFERAY + className;
            Class<?> classToLoad = Util.class.getClassLoader().loadClass(classPath);
            return classToLoad.newInstance();
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }

    public static Class<?> getClassUtil() {
        return new LiferayUtil().getClass();
    }

    public static LiferayUtil getNewLiferayUtil() {
        return new LiferayUtil();
    }
}