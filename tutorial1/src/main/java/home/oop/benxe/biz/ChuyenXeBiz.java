package home.oop.benxe.biz;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import home.oop.benxe.entity.ChuyenXe;
import home.oop.benxe.util.Util;

public class ChuyenXeBiz {

    public static Map<String, ChuyenXe> readChuyenXes(String macx) {
        Map<String, ChuyenXe> mapChuyenXes = new HashMap<>();
        ChuyenXe newChuyenXe = null;
        try {
            JsonArray jsonArray = readChuyenXesJson();
            for (JsonElement chuyenXe : jsonArray) {
                JsonObject aChuyenXe = chuyenXe.getAsJsonObject();
                String idChuyenXe = aChuyenXe.get("maCX").getAsString();
                if (!macx.equals("") && !macx.equals(idChuyenXe)) {
                    continue;
                }
                boolean isNoiThanh = aChuyenXe.get("isNoiThanh").getAsBoolean();
                Object typeChuyenXe = Util.getTypeChuyenXe(isNoiThanh);
                newChuyenXe = (ChuyenXe) typeChuyenXe;
                newChuyenXe.callChuyenXe(aChuyenXe);
                mapChuyenXes.put(newChuyenXe.getMaCX(), newChuyenXe);
            }
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return mapChuyenXes;
    }

    public static JsonArray readChuyenXesJson() {
        JsonArray jsonArray = new JsonArray();
        try {
            String contentChuyenXe = Util.getNewUtil().getInputStreamFromResources("/xes/xes.json");
            JsonParser parse = new JsonParser();
            JsonElement jsonElement = parse.parse(contentChuyenXe);
            JsonElement jsonChuyenXes = jsonElement.getAsJsonObject().get("chuyenXe");
            if(jsonChuyenXes == null){
                return jsonArray;
            }
            jsonArray = jsonChuyenXes.getAsJsonArray();
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return jsonArray;
    }

    // Mode = 1: create
    // Mode = 2: update
    // Mode = 3: del
    public static Boolean changeChuyenXes(ChuyenXe chuyenXe, Integer mode) {
        Boolean isSuccess = false;
        File fileChuyenXeJson = Util.getNewUtil().getFileFromResources("xes/xes.json");
        if (fileChuyenXeJson != null && fileChuyenXeJson.exists()) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            Map<String, ChuyenXe> mapChuyenXes = readChuyenXes("");

            switch (mode) {
            case 1:
                mapChuyenXes.put(chuyenXe.getMaCX(), chuyenXe);
                break;
            case 2:
                ChuyenXe updateXe = mapChuyenXes.get(chuyenXe.getMaCX());
                if (updateXe != null)
                    mapChuyenXes.replace(chuyenXe.getMaCX(), chuyenXe);
                break;
            case 3:
                ChuyenXe delXe = mapChuyenXes.get(chuyenXe.getMaCX());
                if (delXe != null)
                    mapChuyenXes.remove(chuyenXe.getMaCX());
                break;
            }
            JsonObject chuyenXeJsonStructure = createChuyenXesJson(gson, mapChuyenXes);
            try {
                Writer writer = new FileWriter(fileChuyenXeJson.getAbsolutePath());
                gson.toJson(chuyenXeJsonStructure, writer);
                writer.flush();
                writer.close();
                isSuccess = true;
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }
        return isSuccess;
    }

    private static JsonObject createChuyenXesJson(Gson gson, Map<String, ChuyenXe> mapChuyenXes) {
        JsonObject chuyenXes = new JsonObject();
        JsonArray chuyenXe = createChuyenXeJson(mapChuyenXes);

        // jsonObject add jsonArray use add('key', values)
        chuyenXes.add("chuyenXe", chuyenXe);
        return chuyenXes;
    }

    private static JsonArray createChuyenXeJson(Map<String, ChuyenXe> mapChuyenXes) {
        JsonArray chuyenXes = new JsonArray();
        Iterator<Map.Entry<String, ChuyenXe>> iterator = mapChuyenXes.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, ChuyenXe> mapChuyenXe = iterator.next();
            ChuyenXe chuyenXe = mapChuyenXe.getValue();
            JsonObject jsonObjectChuyenXe = new JsonObject();
            chuyenXe.bookChuyenXe(jsonObjectChuyenXe);
            chuyenXes.add(jsonObjectChuyenXe);
        }
        return chuyenXes;
    }

    public static Boolean removeChuyenXes(String maCX, Integer mode) {
        boolean isResult = false;
        Map<String, ChuyenXe> chuyenXes = readChuyenXes(maCX);
        if (!chuyenXes.isEmpty()) {
            ChuyenXe chuyenXe = (ChuyenXe) chuyenXes.values().toArray()[0];
            isResult = changeChuyenXes(chuyenXe, 3);
        }
        return isResult;
    }
}