package run.samples;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import googletools.MergeJson;
import googletools.MergeJson.ConflictStrategy;
import googletools.MergeJson.JsonObjectExtensionConflictException;
import util.Util;

public class test1 {

    public static void main(String[] args) {

        JsonObject jsonObject1 = Util.getNewUtil().convertStringToJson("/googletools/mergejson/json1.json");
        JsonObject jsonObject2 = Util.getNewUtil().convertStringToJson("/googletools/mergejson/json2.json");

        try {
            MergeJson.extendJsonObject(jsonObject1, ConflictStrategy.PREFER_FIRST_OBJ, jsonObject2);
        } catch (JsonObjectExtensionConflictException e) {
            e.printStackTrace();
            System.out.println(e);
        }

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String prettyJsonString = gson.toJson(jsonObject1);
        System.out.println(prettyJsonString);
    }






}